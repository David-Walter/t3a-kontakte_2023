import { ref } from 'vue';
import { defineStore } from 'pinia';
import { useKontakteStore } from './kontakte.js';

export const useModalDeleteStore = defineStore('modalDelete', () => {
    const kontakte = useKontakteStore();
    // D A T A
    const isVisible = ref(false);

    const vorname = ref('-');
    const nachname = ref('-');
    const id = ref(0);

    function show(kontakt) {
        // Kontaktdaten übernehmen
        id.value = kontakt.id;
        vorname.value = kontakt.vorname;
        nachname.value = kontakt.nachname;

        // Fenster sichtbar machen
        isVisible.value = true;
    }

    function buttonCancelClick() {
        isVisible.value = false;
    }

    function buttonDeleteClick() {
        // Kontakt löschen
        kontakte.deleteKontakt(id.value);

        // Fenster unsichtbar machen
        isVisible.value = false;
    }

    return {
        isVisible,
        vorname,
        nachname,
        show,
        buttonCancelClick,
        buttonDeleteClick
    };
});