import { ref } from 'vue';
import { defineStore } from 'pinia';
import { useKontakteStore } from './kontakte.js';

export const useModalCreateStore = defineStore('modalCreate', () => {
    const kontakte = useKontakteStore();
    // D A T A
    const isVisible = ref(false);

    const vorname = ref('-');
    const nachname = ref('-');
    const bot = ref(true);
    const gender = ref('w');
    const haircolor = ref('grün');


    function show() {
        // Default-Werte setzen
        vorname.value = 'Max';
        nachname.value = 'Mustermann';
        bot.value = true;
        gender.value = 'w';

        // Fenster sichtbar machen
        isVisible.value = true;
    }

    function buttonCancelClick() {
        isVisible.value = false;
    }

    function buttonSaveClick() {
        // Neuen Kontakt speichern
        kontakte.createKontakt({
            vorname: vorname.value,
            nachname: nachname.value,
            bot: bot.value,
            gender: gender.value
        });

        // Fenster unsichtbar machen
        isVisible.value = false;
    }

    return {
        isVisible,
        vorname,
        nachname,
        bot,
        gender,
        haircolor,
        show,
        buttonCancelClick,
        buttonSaveClick
    };
});