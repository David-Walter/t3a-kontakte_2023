import { ref } from 'vue';
import { defineStore } from 'pinia';
import { useKontakteStore } from './kontakte.js';

export const useModalUpdateStore = defineStore('modalUpdate', () => {
    const kontakte = useKontakteStore();
    // D A T A
    const isVisible = ref(false);

    const id = ref('-');
    const vorname = ref('-');
    const nachname = ref('-');
    const bot = ref(true);
    const gender = ref('w');
    const haircolor = ref('grün')


    function show(kontakt) {
        // Daten anzeigen
        id.value = kontakt.id;
        vorname.value = kontakt.vorname;
        nachname.value = kontakt.nachname;
        bot.value = kontakt.bot;
        gender.value = kontakt.gender;
        haircolor.value = kontakt.haircolor;

        // Fenster sichtbar machen
        isVisible.value = true;
    }

    function buttonCancelClick() {
        isVisible.value = false;
    }

    function buttonSaveClick() {
        // Bearbeiteten Kontakt speichern
        kontakte.updateKontakt({
            id: id.value,
            vorname: vorname.value,
            nachname: nachname.value,
            bot: bot.value,
            gender: gender.value,
            haircolor: kontakt.haircolor
        });

        // Fenster unsichtbar machen
        isVisible.value = false;
    }

    return {
        isVisible,
        vorname,
        nachname,
        bot,
        gender,
        haircolor,
        show,
        buttonCancelClick,
        buttonSaveClick
    };
});